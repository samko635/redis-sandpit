# redis-sandpit
This is meant to be deployed to Kubernetes cluster using ArgoCD

```bash
# Run redis cluster
docker-compose -f docker-compose up -d

# Run redis stack with RedisInsight
docker run -d --rm --net mynetwork --name redis-stack -p 6379:6379 -p 8001:8001 -v `pwd`/configs/redis-0/redis.conf:/redis-stack.conf -v `pwd`/data/:/data redis/redis-stack:latest
```
